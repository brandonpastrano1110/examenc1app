package com.example.examencorte1;

import android.os.Bundle;
import android.view.View;
import android.widget.*;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CuentaBancoActivity extends AppCompatActivity {
    private TextView lblNombre;
    private EditText txtNumCuenta;
    private EditText txtNombre;
    private EditText txtBanco;
    private EditText txtSaldo;
    private EditText txtCantidad;
    private Spinner spinnerMovimientos;
    private Button btnConsultarSaldo;
    private Button btnAplicar;
    private Button btnLimpiar;
    private Button btnRegresar;

    private CuentaBanco cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cuenta_banco);
        iniciarComponentes();
        cuenta = new CuentaBanco("123456", "Admin", "Banco", 1000.0f);

        btnConsultarSaldo.setOnClickListener(v -> {
            if (isEditTextEmpty(txtNumCuenta) || isEditTextEmpty(txtNombre) || isEditTextEmpty(txtBanco) || isEditTextEmpty(txtSaldo) || isEditTextEmpty(txtCantidad)) {
                Toast.makeText(CuentaBancoActivity.this, "Por favor llene todos los campos", Toast.LENGTH_SHORT).show();
            }
            else{
                txtSaldo.setText(String.valueOf(cuenta.obtenerSaldo()));
            }

        });
        lblNombre.setText(cuenta.getNombre());
        Spinner spinnerMovimientos = findViewById(R.id.spinnerMovimientos);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.movimientos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMovimientos.setAdapter(adapter);
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String seleccion = spinnerMovimientos.getSelectedItem().toString();
                if (seleccion.equals("Retirar")) {
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    if (isEditTextEmpty(txtNumCuenta) || isEditTextEmpty(txtNombre) || isEditTextEmpty(txtBanco) || isEditTextEmpty(txtSaldo) || isEditTextEmpty(txtCantidad)) {
                        Toast.makeText(CuentaBancoActivity.this, "Por favor llene todos los campos", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        if(cuenta.retirarDinero(cantidad)){
                            Toast.makeText(CuentaBancoActivity.this, "Se retiro con exito", Toast.LENGTH_SHORT).show();
                            txtSaldo.setText(String.valueOf(cuenta.obtenerSaldo()));
                        }
                        else {
                            Toast.makeText(CuentaBancoActivity.this, "Dinero Insuficiente", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (seleccion.equals("Depositar")) {
                    // Realizar la lógica para depositar dinero
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    if (isEditTextEmpty(txtNumCuenta) || isEditTextEmpty(txtNombre) || isEditTextEmpty(txtBanco) || isEditTextEmpty(txtSaldo) || isEditTextEmpty(txtCantidad)) {
                        Toast.makeText(CuentaBancoActivity.this, "Por favor llene todos los campos", Toast.LENGTH_SHORT).show();
                    } else {
                        cuenta.hacerDeposito(cantidad);
                        txtSaldo.setText(String.valueOf(cuenta.obtenerSaldo()));
                    }
                }
            }
        });


        btnLimpiar.setOnClickListener(v -> {
            txtNumCuenta.setText("");
            txtNombre.setText("");
            txtBanco.setText("");
            txtSaldo.setText("");
            txtCantidad.setText("");
        });
        btnRegresar.setOnClickListener(v -> {
            finish();
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        lblNombre = findViewById(R.id.lblNombre);
        txtNumCuenta = findViewById(R.id.txtNumCuenta);
        txtNombre = findViewById(R.id.txtNombre);
        txtBanco = findViewById(R.id.txtBanco);
        txtSaldo = findViewById(R.id.txtSaldo);
        txtCantidad = findViewById(R.id.txtCantidad);
        spinnerMovimientos = findViewById(R.id.spinnerMovimientos);
        btnConsultarSaldo = findViewById(R.id.btnConsultarSaldo);
        btnAplicar=findViewById(R.id.btnAplicar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
    }
    private boolean isEditTextEmpty(EditText editText) {
        return editText.getText().toString().trim().isEmpty();
    }
}